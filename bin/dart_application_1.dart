import 'dart:io';

import 'package:dart_application_1/dart_application_1.dart'
    as dart_application_1;

void main() {
  print("Select the choice you want to perform : ");
  print("1. ADD");
  print("2. SUBTRACT");
  print("3. MULTIPLY");
  print("4. DIVIDE");
  print("5. EXIT");

  print("Choice you want to enter : ");
  var choice = stdin.readLineSync();

  switch (choice) {
    case '1': 
    {
      print("Enter the value for x : ");
      int? x = int.parse(stdin.readLineSync()!);

      print("Enter the value for y : ");
      int? y = int.parse(stdin.readLineSync()!);

      print("Sum of the two numbers is : ");
      print(x+y);
    }
      break;
    case '2':
    {
      print("Enter the value for x : ");
      int? x = int.parse(stdin.readLineSync()!);

      print("Enter the value for y : ");
      int? y = int.parse(stdin.readLineSync()!);

      print("Difference of the two numbers is : ");
      print(x-y);
    }
      break;
      case '3':
    {
      print("Enter the value for x : ");
      int? x = int.parse(stdin.readLineSync()!);

      print("Enter the value for y : ");
      int? y = int.parse(stdin.readLineSync()!);

      print("Multiply of the two numbers is : ");
      print(x*y);
    }
      break;
      case '4': 
    {
      print("Enter the value for x : ");
      int? x = int.parse(stdin.readLineSync()!);

      print("Enter the value for y : ");
      int? y = int.parse(stdin.readLineSync()!);

      print("Quotient of the two numbers is : ");
      print(x/y);
    }
      break;
      case '5': 
    {
      print("EXIT");
      break;
    }

  }

}
